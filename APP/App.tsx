/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useCallback, useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import DeviceList from './DeviceList';
import { bleManager } from './manager';
import { settingUUID, dataUUID, serviceUUID } from './constants';
import { Base64 } from 'js-base64';
import { Subscription } from 'react-native-ble-plx';

function hexToBase64(hexstring: string) {
  return Base64.btoa((hexstring.match(/\w{2}/g) || []).map(function(a) {
      return String.fromCharCode(parseInt(a, 16));
  }).join(""));
}
function toNumber(bytes: Uint8Array) {
  if (bytes[0] === 255 && bytes[1] === 255) {
    return '-';
  }
  return (
         ((bytes[0] >> 4) * 1000) +
         (bytes[0] % 16 * 100) +
         ((bytes[1] >> 4) * 10) +
         (bytes[1] % 16)) + '';
}

const App = () => {
  const [bluetoothState, setBluetoothState] = useState('Unknown')

  useEffect(() => {
    const subscription = bleManager.onStateChange((state) => {
      setBluetoothState(state);
      if (state === 'PoweredOn') {
        subscription.remove();
      }
    }, true);

    return () => {
      subscription.remove();
    }
  }, [])

  const [temperature, setTemperature] = useState('');
  const [heartRate, setHeartRate] = useState('');
  const [spo2, setSpo2] = useState('');
  const [bloodPressureS, setBloodPressureS] = useState('');
  const [bloodPressureD, setBloodPressureD] = useState('');

  const [monitoring, setMonitoring] = useState<Subscription | null>(null);
  const onSelectDevice = useCallback(async device => {
    await device.connect()
    await device.discoverAllServicesAndCharacteristics()
    await bleManager.writeCharacteristicWithResponseForDevice(device.id, serviceUUID, settingUUID, hexToBase64('05'))
    setMonitoring(bleManager.monitorCharacteristicForDevice(device.id, serviceUUID, dataUUID, (err, char) => {
      if (err) {
        return;
      }

      if (char?.value) {
        const data = Base64.toUint8Array(char?.value)

        setTemperature(toNumber(data.slice(0, 2)))
        setHeartRate(toNumber(data.slice(2, 4)))
        setSpo2(toNumber(data.slice(4, 6)))
        setBloodPressureS(toNumber(data.slice(6, 8)))
        setBloodPressureD(toNumber(data.slice(8, 10)))
      }
    }));
  }, [setMonitoring])

  useEffect(() => {
    return () => {
      monitoring?.remove();
    }
  }, [monitoring])

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic">
          <View>
            <Text>Bluetooth State: {bluetoothState}</Text>
          </View>

          {
            bluetoothState === 'PoweredOn' && (
              <>
                <DeviceList onSelectDevice={onSelectDevice} />
              </>
            )
          }

          {
            monitoring != null && (
              <>
                <View>
                  <Text>temperature {temperature}</Text>
                  <Text>heart rate {heartRate}</Text>
                  <Text>spo2 {spo2}</Text>
                  <Text>blood pressure S {bloodPressureS}</Text>
                  <Text>blood pressure D {bloodPressureD}</Text>
                </View>
              </>
            )
          }
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  engine: {
    position: 'absolute',
    right: 0,
  },
});

export default App;
