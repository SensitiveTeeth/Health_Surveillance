import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native';
import { Device } from 'react-native-ble-plx';
import { bleManager } from './manager';

export default function DeviceList(props: {
  onSelectDevice?: (device: Device) => void;
}) {
  const [devices, setDevices] = useState<Device[]>([]);

  useEffect(() => {
    bleManager.startDeviceScan(null, null, (error, device) => {
      if (error ) {
        console.log(error)
        return
      }
      if (device == null) {
        return
      }
      if (device.name !== "TECH HEALTH") {
        return
      }

      setDevices(devices => {
        if (devices.findIndex(d => d.id === device.id) > -1) {
          return devices;
        } else {
          return [...devices, device]
        }
      })
    });

    return () => {
      bleManager.stopDeviceScan();
    }
  }, [])

  return (
    <>
      {devices.map(device => (
        <TouchableOpacity onPress={() => {
          props.onSelectDevice?.(device);
        }} key={device.id}>
          <View style={{padding: 10}}>
            <Text>{device.localName}</Text>
            <Text>{device.id}</Text>
          </View>
        </TouchableOpacity>
      ))}
    </>
  )
}