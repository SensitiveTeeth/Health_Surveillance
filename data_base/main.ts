import { AuthController } from './controllers/AuthController'
import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import Knex from 'knex'
import { createIsLoggedIn } from './guard'
import { Bearer } from 'permit'

// import multer from 'multer'

// const upload = multer({ dest: 'uploads/' })

dotenv.config()
const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])



const app = express()



app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors({
  origin: [
    process.env.REACT_DOMAIN!
  ]
}))

// above is setting
// service from here 

declare global {
  namespace Express {
    interface Request {
      user?: {
        id: number | string;
        username: string;
      }
    }
  }
}


import { UsersServices } from './services/UsersServices';
import { PagesServices } from './services/PagesServices'
import { UsersController } from './controllers/UsersController';
import { PagesController } from './controllers/PagesController';
import { createRouter } from './routes'



const usersServices = new UsersServices(knex);
export const usersController = new UsersController(usersServices);

export const authController = new AuthController(usersServices);

const pagesServices = new PagesServices(knex);
export const pagesController = new PagesController(pagesServices);

const permit = new Bearer({
  query: "access_token"
})

export const isLoggedIn = createIsLoggedIn(permit, usersServices)

let router = createRouter({
  isLoggedIn,
  authController,
  pagesController,
  usersController
})
app.use(router);






const PORT = process.env.PORT || 8000
app.listen(PORT, () => {
  console.log('Listening at port ' + PORT)
})
