import * as Knex from "knex";

// create new table
export async function up(knex: Knex) {
  await knex.schema.createTable("admin", (table) => {
    table.increments();
    table.string("usercode").notNullable();
    table.string("username");
    table.string("password");
    table.string("position");
  });

  await knex.schema.createTable("normal_users", (table) => {
    // table.string("photo");
    table.increments();
    table.string("usercode",8).notNullable();
    table.string("chinese_name", 255);
    table.string("english_name", 255);
    table.string("home_number");
    table.string("contact_person", 255);
    table.string("contact_person_number");
    table.string("emergency_contact", 255);
    table.string("emergency_contact_number");
    table.string("emergency_contact2", 255);
    table.string("emergency_contact_number2");
    table.string("emergency_contact3", 255);
    table.string("emergency_contact_number3");
    table.string("gender");
    table.string("doctor_in_charge")
    table.string("drug_allergy")
    table.string("food_allergy")
    table.string("drug_history")
    table.string("medical_history")
    table.string("hospital_history")
    table.string("default_systolic_blood_pressure_top_level");
    table.string("default_systolic_blood_pressure_low_level");
    table.string("default_diastolic_blood_pressure_top_level");
    table.string("default_diastolic_blood_pressure_low_level");
    table.string("default_blood_oxygen_contact_top_level");
    table.string("default_blood_oxygen_contact_low_level");
    table.string("default_heart_rate_top_level");
    table.string("default_heart_rate_low_level");
    table.timestamps(false, true)

  });

  await knex.schema.createTable("bp_history", (table) => {
    table.increments();
    table.integer("users_id").unsigned();
    table.foreign("users_id").references("normal_users.id")
    table.integer("followed_by").unsigned();
    table.foreign("followed_by").references("admin.id")
    table.timestamps(false, true)
    //data
    table.string("measured_systolic_blood_pressure");
    table.string("measured_diastolic_blood_pressure");
    table.string("measured_blood_oxygen_contact");
    table.string("measured_heart_rate");
    table.boolean("measured_systolic_blood_pressure_out_range");
    table.boolean("measured_diastolic_blood_pressure_out_range");
    table.boolean("measured_blood_oxygen_contact_out_range");
    table.boolean("measured_heart_rate_out_range");
    //followup
    table.boolean("needed_to_follow_up_list")
    table.boolean("been_followed")
    table.boolean("not_measured_list")
    table.boolean("measured_list")
    

  })

  await knex.schema.createTable("remark", (table) =>{
    table.increments();
    table.integer("bp_history_id").unsigned();
    table.foreign("bp_history_id").references("bp_history.id")
    table.string('text_remark')
    table.timestamps(false,true)
  })
  // await knex.schema.createTable("bp_case",(table)=>{
  //   table.increments();
  //   table.integer("bp_history_id").unsigned();
  //   table.foreign("bp_history_id").references("bp_history.id");
  //   table.integer("users_id").unsigned();
  //   table.foreign("users_id").references("normal_users.id");
    // table.boolean("case_followed")
    // table.string("case_remark")
    // table.integer("case_followed_by").unsigned();
    // table.foreign("case_followed_by").references("admin.id")
  //   table.string("needed_to_follow_up_list")
  //   table.string("not_measured_list")
  //   table.string("measured_list")
  // })
}


//roll back delete all table
export async function down(knex: Knex) {
  await knex.schema.dropTableIfExists('remark');
  await knex.schema.dropTableIfExists('bp_history');
  await knex.schema.dropTableIfExists('normal_users');
  await knex.schema.dropTableIfExists('admin');

};