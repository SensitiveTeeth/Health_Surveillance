import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable("admin",(table)=>{
        table.string("usercode").notNullable().unique().alter();
    })
    await knex.schema.alterTable("normal_users",(table)=>{
        table.string("usercode").notNullable().unique().alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("admin",(table)=>{
        table.string("usercode").notNullable().alter();
    })
    await knex.schema.alterTable("normal_users",(table)=>{
        table.string("usercode").notNullable().alter();
    })
}

