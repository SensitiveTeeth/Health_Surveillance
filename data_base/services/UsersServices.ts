import * as Knex from 'knex';
import { Admin, NormalUser } from './model';


export class UsersServices {
    constructor(private knex: Knex) { }
    //get by userCode(login) 已完成
    async getCurrentCodeService(usercode: string): Promise<Admin> {
        const result:Admin[] = await this.knex
            .select('*')
            .from('admin')
            .where('usercode', '=', usercode)
        return result[0]
        // return (await this.knex.raw(`SELECT * FROM admin WHERE usercode = ?`, [usercode])).rows[0];
    }
    async getCurrentUserByIdService(id: number): Promise<Admin> {
        const result:Admin[] = await this.knex
            .select('*')
            .from('admin')
            .where('id', '=', id)
        return result[0]
        // return (await this.knex.raw(`SELECT * FROM admin WHERE id = ?`, [id])).rows[0];
    }
    //


    //read remark by bp_history id
    async getRemarkByIdService(BpHistoryID: number) {
        const id = BpHistoryID
        return await this.knex
            .select('*')
            .from('remark')
            .where('bp_history_id', id)
            .orderBy('created_at', 'asc')
    }
    //create remark
    async createRemark(bp_history_id: number, text_remark: string) {
        await this.knex
            .insert({
                bp_history_id,
                text_remark
            })
            .into('remark')
    }

    async getAdminUserCodesService() {
        const result = await this.knex
            .select('usercode')
            .from('admin')
        return result;
    }
    //get oldPassword From Admin
    async getAdminPassword(id: number) {
        return (await this.knex.raw(/*sql*/`SELECT password FROM admin WHERE id = ?`, [id])).rows[0];

    }

    //get normal_users detail
    async getNormalUserDetailByIdService(id: number) {
        return (await this.knex.raw(/*sql*/`SELECT * from normal_users WHERE id = ?`, [id])).rows
    }

    //create admin
    async createAdminService(id: number, usercode: string, hashedPassword: string, username: string, position: string) {
        const password = hashedPassword
        await this.knex
            .insert({
                id,
                usercode,
                password,
                username,
                position,
            })
            .into('admin')
    }
    //update admin
    async updateAdminService(id: number, usercode: string, hashPassword: string, username: string, position: string) {
        const password = hashPassword
        return await this.knex('admin').update({
            usercode,
            username,
            password,
            position,
        })
            .where('id', id)
            .returning('id');
    }
    //get admin by id 
    async getAdminByIdService(id: number) {
        // return (await this.knex.raw(`SELECT * FROM admin WHERE id = ?`, [id])).rows[0];
        return await this.knex
            .select('*')
            .from('admin')
            .where('id', id)
    }

    //get bp_history by id
    async getBp_HistoryByIdService(page: string, id: number) {
        const perPage = (5)
        const pageNum = (parseInt((page || '1') + '') - 1) * perPage
        const bp_history = await this.knex
            .select(
                'bp_history.id',
                'normal_users.chinese_name',
                'measured_systolic_blood_pressure',
                'measured_diastolic_blood_pressure',
                'measured_blood_oxygen_contact',
                'measured_heart_rate',
                "bp_history.created_at",
                'measured_systolic_blood_pressure_out_range',
                'measured_diastolic_blood_pressure_out_range',
                'measured_blood_oxygen_contact_out_range',
                'measured_heart_rate_out_range',
                'needed_to_follow_up_list',
                'been_followed',
            )
            .from('bp_history')
            .leftJoin('normal_users', 'bp_history.users_id', '=', 'normal_users.id')
            .where('bp_history.users_id', '=', id)
            .limit(perPage)
            .offset(pageNum)
            .orderBy('bp_history.created_at', 'desc')
        const count = await this.knex
            .count('bp_history')
            .leftJoin('normal_users', 'bp_history.users_id', '=', 'normal_users.id')
            .from('bp_history')
            .where('bp_history.users_id', '=', id)

        return { bp_history, count };


    }
    //c for create user
    async createUser(
        user: NormalUser
    ) {

        await this.knex
            .insert(user)
            .into("normal_users")

    }
    //u for update user
    async updateUserService(user: NormalUser) {

        return await this.knex('normal_users').update(user).where('id', user.id)
            .returning('id');
    }
    async deleteUserService(id: number) {
        return await this.knex('normal_users').where('id', id).del();
    }
}

