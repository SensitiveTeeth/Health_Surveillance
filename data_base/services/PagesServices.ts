import * as Knex from 'knex';
import {Followup, Colleague, UserManage } from './model'

export class PagesServices {
    constructor(private knex: Knex) { }
    //followup page and user managed page
    public async getFollowUpPageService(page: string) {
        const perPage = (7)
        const pageNum = (parseInt((page || '1') + '') - 1) * perPage
        const normal_user = await this.knex
            .select<Followup>(
                'bp_history.id',
                'normal_users.chinese_name',
                'measured_systolic_blood_pressure',
                'measured_diastolic_blood_pressure',
                'measured_blood_oxygen_contact',
                'measured_heart_rate',
                'normal_users.english_name',
                'normal_users.gender',
                'bp_history.created_at as date'
            )
            .from('normal_users')
            .leftJoin('bp_history', 'bp_history.users_id', '=', 'normal_users.id')
            .limit(perPage)
            .offset(pageNum);
        const count = await this.knex
            .count<number>('normal_users.id')
            .leftJoin('bp_history', 'bp_history.users_id', '=', 'normal_users.id')
            .from('normal_users')
        return { normal_user, count };
    }
    //ColleaguePage
    public async getColleaguePageService(page: string) {
        const perPage = (7)
        const pageNum = (parseInt((page || '1') + '') - 1) * perPage
        const colleague = await this.knex
            .select<Colleague>(
                'admin.id',
                'username',
                'position',
                'usercode')
            .from('admin')
            .limit(perPage)
            .offset(pageNum);

        const count = await this.knex
            .count<number>('admin.id')
            .from('admin')
        return { colleague, count };
    }

    public async getUserPageService(page: string){
        const perPage = (7)
        const pageNum = (parseInt((page || '1') + '') - 1) * perPage
        const user_page = await this.knex
            .select<UserManage>(
                'normal_users.id',
                'chinese_name',
                'english_name',
                'gender',
            )
            .from('normal_users')
            .limit(perPage)
            .offset(pageNum)
            .orderBy('normal_users.id')
        const count = await this.knex
                .count<number>('normal_users.id')
                .from('normal_users')
        return {user_page, count};

    }


}