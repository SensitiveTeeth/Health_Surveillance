export interface UserManage {
    id: number;
    chinese_name: string;
    english_name: string;
    gender: string;
}
export interface Cases {
    user_id: number;
    bp_history_id: number;
    case_followed: boolean;
    case_remark: string;
    case_followed_by: number;
}

export interface Followup {
    id: number;
    chinese_name: string;
    measured_systolic_blood_pressure: string;
    measured_diastolic_blood_pressure: string;
    measured_blood_oxygen_contact: string;
    measured_heart_rate: string;
    date: string;
    english_name: string;
    gender: string;
}

export interface Admin {
    id: string
    usercode: string
    password?: string
    username: string
    position: string
}


export interface Colleague {
    username: string,
    position: string,
    usercode: string,
}

export interface NormalUser {
    id: number,
    usercode: string,
    home_number: string,
    chinese_name: string,
    english_name: string,
    contact_person: string,
    contact_person_number: string,
    emergency_contact: string,
    emergency_contact_number: string,
    emergency_contact2: string,
    emergency_contact_number2: string,
    emergency_contact3: string,
    emergency_contact_number3: string,
    doctor_in_charge: string,
    gender: string,
    drug_allergy: string,
    food_allergy: string,
    drug_history: string,
    medical_history: string,
    hospital_history: string,
    default_systolic_blood_pressure_top_level: string,
    default_systolic_blood_pressure_low_level: string,
    default_diastolic_blood_pressure_top_level: string,
    default_diastolic_blood_pressure_low_level: string,
    default_blood_oxygen_contact_top_level: string,
    default_blood_oxygen_contact_low_level: string,
    default_heart_rate_top_level: string,
    default_heart_rate_low_level: string,
}