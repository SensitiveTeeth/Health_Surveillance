import { Request, Response } from 'express'
import { UsersServices } from '../services/UsersServices'
import { hashPassword, checkPassword } from '../hash'
import { NormalUser } from '../services/model';

export class UsersController {
    constructor(private usersService: UsersServices) {}
    //get Current User
    getCurrentUser = (req: Request, res: Response) => {
        return res.json(req.user);
    }

    getUserByID = async (req: Request, res: Response) => {
        try {
            const userIDStr = req.params.id;
            const userID = parseInt(userIDStr)
            return res.json(await this.usersService.getCurrentUserByIdService(userID));
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }

    getRemarkByBpHistoryId = async (req: Request, res: Response) => {
        try {
            const BpHistoryIDStr = req.params.id;
            const BpHistoryID = parseInt(BpHistoryIDStr)
            return res.json(await this.usersService.getRemarkByIdService(BpHistoryID))
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }

    getBp_HistoryById = async (req: Request, res: Response) => {
        try {
            const page = req.query.page as string;
            const bp_HistoryIdStr = req.params.id;
            const bp_HistoryId = parseInt(bp_HistoryIdStr)
            return res.json(await this.usersService.getBp_HistoryByIdService(page, bp_HistoryId));

        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }

    getNormalUserDetailById = async (req: Request, res: Response) => {
        try {
            const normal_userIdStr = req.params.id;
            const normal_userId = parseInt(normal_userIdStr)
            return res.json(await this.usersService.getNormalUserDetailByIdService(normal_userId));

        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })

        }
    }
    postRemark = async (req: Request, res: Response) => {
        try {
            const { remark } = req.body
            const bp_history_id = parseInt(req.params.id)
            await this.usersService.createRemark(bp_history_id, remark)
            return res.status(200).json({ message: '成功' })
        } catch (e) {
            console.error(e);
            return res.status(500).json({ message: 'internal service error' })

        }
    }



    //create New Admin
    postAdmin = async (req: Request, res: Response) => {
        try {
            const { id, usercode, password, username, position, } = req.body
            const checkIsNum = parseInt(req.body.usercode)
            if ((checkIsNum).toString().length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            if (req.body.usercode.length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            if (!req.body.usercode || !req.body.password || !req.body.username || !req.body.position) {
                return res.status(400).json({ message: '請輸入所有資料' })
            }
            const checkUserCodes = await this.usersService.getAdminUserCodesService();
            for (let checkUserCode of checkUserCodes) {
                if (req.body.usercode === checkUserCode.usercode) {
                    return res.status(400).json({ message: '此號碼已被使用' })
                }
            }
            const hashedPassword = await hashPassword(password)
            await this.usersService.createAdminService(id, usercode, hashedPassword, username, position,);
            return res.status(200).json({ message: '成功' })
        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }
    //update Admin
    putAdmin = async (req: Request, res: Response) => {
        try {
            const { usercode, password, username, position, oldPassword } = req.body
            const hashedPassword = await hashPassword(password)
            const id = parseInt(req.params.id);
            if (!req.params.id) {
                return res.status(400).json({ message: '我個野壞左呀' })
            }
            const checkIsNum = parseInt(req.body.usercode)
            if (!req.body.usercode || !req.body.password || !req.body.username || !req.body.position) {
                return res.status(400).json({ message: '請輸入所有資料' })
            }
            if ((checkIsNum).toString().length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            if (req.body.usercode.length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            const checkAdminPassword = await this.usersService.getAdminPassword(id);
            if (await checkPassword(oldPassword, checkAdminPassword.password) !== true) {
                return res.status(403).json({ message: '舊有密碼不正確' })
            }
            if (isNaN(id)) {
                return res.status(400).json({ message: "ID is not a number" })
            }
            const updated = await this.usersService.updateAdminService(id, usercode, hashedPassword, username, position,)
            return res.json({ updated });
        } catch (err) {
            if (err.constraint == 'admin_usercode_unique') {
                return res.status(400).json({ message: '電話號碼已被使用' })
            }
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }



    //get admin by id
    getAdminById = async (req: Request, res: Response) => {
        try {
            const adminIdStr = req.params.id;
            const adminId = parseInt(adminIdStr)
            return res.json(await this.usersService.getAdminByIdService(adminId));


        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }


    //create New User
    postUser = async (req: Request, res: Response) => {
        try {
            const user: NormalUser = req.body
            if (
                !user.usercode ||
                !user.home_number ||
                !user.chinese_name ||
                !user.english_name ||
                !user.contact_person ||
                !user.contact_person_number ||
                !user.emergency_contact ||
                !user.emergency_contact_number ||
                !user.emergency_contact2 ||
                !user.emergency_contact_number2 ||
                !user.emergency_contact3 ||
                !user.emergency_contact_number3 ||
                !user.doctor_in_charge ||
                !user.gender ||
                !user.drug_allergy ||
                !user.food_allergy ||
                !user.drug_history ||
                !user.medical_history ||
                !user.hospital_history ||
                !user.default_systolic_blood_pressure_top_level ||
                !user.default_systolic_blood_pressure_low_level ||
                !user.default_diastolic_blood_pressure_top_level ||
                !user.default_diastolic_blood_pressure_low_level ||
                !user.default_blood_oxygen_contact_top_level ||
                !user.default_blood_oxygen_contact_low_level ||
                !user.default_heart_rate_top_level ||
                !user.default_heart_rate_low_level
            ) {
                console.log(req.body)
                return res.status(400).json({ message: '請輸入所有資料' })
            }
            const checkIsNum = parseInt(req.body.usercode)
            if ((checkIsNum).toString().length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            if (req.body.usercode.length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            const checkUserCodes = await this.usersService.getAdminUserCodesService();
            for (let checkUserCode of checkUserCodes) {
                if (req.body.usercode === checkUserCode.usercode) {
                    return res.status(400).json({ message: '此號碼已被使用' })
                }
            }
            await this.usersService.createUser(user);
            return res.status(200).json({ message: '成功' })

        } catch (err) {
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }
    //update User
    putUsers = async (req: Request, res: Response) => {
        const id = parseInt(req.params.id);
        const user: NormalUser = req.body
        user.id = id
        try {
            if (isNaN(id)) {
                res.status(400).json({ msg: "ID is not a number" })
                return;
            }
            if (!req.params.id) {
                return res.status(400).json({ message: '我個野壞左呀' })
            }
            const checkIsNum = parseInt(user.usercode)
            if (
                !user.usercode ||
                !user.home_number ||
                !user.chinese_name ||
                !user.english_name ||
                !user.contact_person ||
                !user.contact_person_number ||
                !user.emergency_contact ||
                !user.emergency_contact_number ||
                !user.emergency_contact2 ||
                !user.emergency_contact_number2 ||
                !user.emergency_contact3 ||
                !user.emergency_contact_number3 ||
                !user.doctor_in_charge ||
                !user.gender ||
                !user.drug_allergy ||
                !user.food_allergy ||
                !user.drug_history ||
                !user.medical_history ||
                !user.hospital_history ||
                !user.default_systolic_blood_pressure_top_level ||
                !user.default_systolic_blood_pressure_low_level ||
                !user.default_diastolic_blood_pressure_top_level ||
                !user.default_diastolic_blood_pressure_low_level ||
                !user.default_blood_oxygen_contact_top_level ||
                !user.default_blood_oxygen_contact_low_level ||
                !user.default_heart_rate_top_level ||
                !user.default_heart_rate_low_level
            ) {
                return res.status(400).json({ message: '請輸入所有資料' })
            }
            if ((checkIsNum).toString().length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            if (req.body.usercode.length !== 8) {
                return res.status(400).json({ message: '登入號碼必需為8位數字' })
            }
            const updated = await this.usersService.updateUserService(user)
            return res.json({ updated });
        } catch (err) {
            if (err.constraint == 'normal_users_usercode_unique') {
                return res.status(400).json({ message: '電話號碼已被使用' })
            }
            console.error(err);
            return res.status(500).json({ message: 'internal service error' })
        }
    }
    deleteUsers = async (req: Request, res: Response) => {
        const id = parseInt(req.params.id);
        try {
            if (isNaN(id)) {
                res.status(400).json({ msg: "ID is not a number" })
                return;
            }
            await this.usersService.deleteUserService(id);
            res.json({ updated: 1 })
        } catch (err) {
            console.error(err);
            res.status(500).json({ message: 'internal service error' })
        }
    }

}
