import express from 'express'
import { UsersServices } from '../services/UsersServices'
import { checkPassword } from '../hash'
import jwtSimple from 'jwt-simple'
import jwt from "../jwt";

export class AuthController {
    constructor(private usersServices: UsersServices) {

    }
    adminLogin = async (req: express.Request, res: express.Response) => {
        try {
            if (!req.body.usercode || !req.body.password) {
                return res.status(400).json({ error: 'Incorrect UserCode or Password' })
            }
            const user = await this.usersServices.getCurrentCodeService(req.body.usercode);
            if (user == null) {
                return res.status(400).json({ error: 'Incorrect UserCode or Password' })
            }
            if (!await checkPassword(req.body.password, user.password!)) {
                return res.status(400).json({ error: 'Incorrect UserCode or Password' })
            }
            const payload = {
                id: user.id
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            return res.json({
                token: token
            })
        } catch (e) {
            console.error(e);
            return res.status(400).json({ error: 'Unknown error' })
        }
    }
}