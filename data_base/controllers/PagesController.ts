import { Request, Response } from 'express'
import { PagesServices } from '../services/PagesServices'


export class PagesController {
    constructor(private pagesService: PagesServices) {
    }
    followUpPage = async (req: Request, res: Response) => {
        const page = req.query.page as string;
        try {
            return res.json(await this.pagesService.getFollowUpPageService(page));
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: 'internal service error' })
        }
    }
    colleaguePage = async (req: Request, res: Response) => {
        const page = req.query.page as string;
        try {
            return res.json(await this.pagesService.getColleaguePageService(page));
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: 'internal service error' })
        }
    }
    userPage = async (req: Request, res: Response) => {
        const page = req.query.page as string;
        try {
            return res.json(await this.pagesService.getUserPageService(page))
        } catch (err) {
            console.error(err);
            return res.status(500).json({ error: 'internal service error' })

        }
    }
}