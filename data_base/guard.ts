import express from 'express';
import { UsersServices } from './services/UsersServices';
import { Bearer } from 'permit';
import jwt from './jwt'
import jwtSimple from 'jwt-simple'

export const createIsLoggedIn = (permit: Bearer, userService: UsersServices) => {
    return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
        try {
            const token = permit.check(req);
            if (!token) {
                return res.status(401).json({ error: "Permission Denied" })
            }
            const payload = jwtSimple.decode(token, jwt.jwtSecret);
            const user = await userService.getCurrentUserByIdService(payload.id)
            if (!user) {
                return res.status(401).json({ error: "Permission Denied" })
            }
            const { password, ...userWithoutPassword } = user;
            req.user = userWithoutPassword;



            return next();
        } catch (e) {
            console.error(e)
            return res.status(401).json({ error: "Permission Denied" })
        }

    }
}