import express from 'express'
import { AuthController } from './controllers/AuthController';
import { PagesController } from './controllers/PagesController';
import { UsersController } from './controllers/UsersController';
// import { isLoggedIn } from './main'

// export const routes = express.Router();
//current user
// routes.get('/user', isLoggedIn, usersController.getCurrentUser);

// routes.post('/login', authController.adminLogin)

// routes.post('/admin', isLoggedIn, usersController.postAdmin);

// routes.put('/admin/:id', isLoggedIn, usersController.putAdmin);

// routes.get('/bp_history/:id', isLoggedIn, usersController.getBp_HistoryById)

// routes.get('/followup', isLoggedIn, pagesController.followUpPage);

// routes.get('/colleague', isLoggedIn, pagesController.colleaguePage);

// routes.get('/user_page', isLoggedIn, pagesController.userPage);

// routes.post('/user', isLoggedIn, usersController.postUser);

// routes.get('/user/:id', isLoggedIn, usersController.getNormalUserDetailById)

// routes.put('/user/:id', isLoggedIn, usersController.putUsers);

// routes.delete('/user/:id', isLoggedIn, usersController.deleteUsers);

// routes.get('/remark/:id', isLoggedIn, usersController.getRemarkByBpHistoryId)

// routes.post('/remark/:id', isLoggedIn, usersController.postRemark)


// routes.get('/single_bp_history/:id',usersController.getBpHistoryByBpHistoryId)

export function createRouter(options: {
  usersController:UsersController,
  authController:AuthController,
  pagesController:PagesController,
  isLoggedIn: express.RequestHandler
}) {
  const router = express.Router()
  const { usersController, authController, pagesController,isLoggedIn } = options

  //current user
router.get('/user', isLoggedIn, usersController.getCurrentUser);

router.post('/login', authController.adminLogin)

router.post('/admin', isLoggedIn, usersController.postAdmin);

router.put('/admin/:id', isLoggedIn, usersController.putAdmin);

router.get('/bp_history/:id', isLoggedIn, usersController.getBp_HistoryById)

router.get('/followup', isLoggedIn, pagesController.followUpPage);

router.get('/colleague', isLoggedIn, pagesController.colleaguePage);

router.get('/user_page', isLoggedIn, pagesController.userPage);

router.post('/user', isLoggedIn, usersController.postUser);

router.get('/user/:id', isLoggedIn, usersController.getNormalUserDetailById)

router.put('/user/:id', isLoggedIn, usersController.putUsers);

router.delete('/user/:id', isLoggedIn, usersController.deleteUsers);

router.get('/remark/:id', isLoggedIn, usersController.getRemarkByBpHistoryId)

router.post('/remark/:id', isLoggedIn, usersController.postRemark)

  return router
}