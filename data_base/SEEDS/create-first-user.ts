import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    await knex('remark').del();
    await knex('bp_history').del();
    await knex('normal_users').del();
    await knex('admin').del();

    const adminIdStr = await knex
        .insert({
            usercode: "99901234",
            username: "李護士",
            password: await hashPassword("12345678"),
            position: "監察員"
        })
        .returning('id')
        .into("admin")
    const adminId = adminIdStr.toString()

    // const adminId2Str = 
    await knex
        .insert({
            usercode: "99912345",
            username: "呂護士",
            password: await hashPassword("12345678"),
            position: "監察員"
        })
        .into("admin")
        .returning('id')
    // const adminId2 = adminId2Str.toString()

    // const adminId3Str = 
    await knex
        .insert({
            usercode: "99923456",
            username: "藍護士",
            password: await hashPassword("12345678"),
            position: "義工"
        })
        .into("admin")
        .returning('id')
    // const adminId3 = adminId3Str.toString()



    //user1
    const [userID] =
        await knex
            .insert({
                usercode: "99901235",
                chinese_name: "陳大文",
                english_name: "Chan Tai Man",
                home_number: "23456789",
                contact_person: "陳一一",
                contact_person_number: "29876543",
                emergency_contact: "123",
                emergency_contact_number: "43572896",
                emergency_contact2: 'N/A',
                emergency_contact_number2: 'N/A',
                emergency_contact3: 'N/A',
                emergency_contact_number3: 'N/A',
                gender: '男',
                doctor_in_charge: 'N/A',
                drug_allergy: 'N/A',
                food_allergy: 'N/A',
                drug_history: 'N/A',
                medical_history: 'N/A',
                hospital_history: 'N/A',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",

        })
        .into('bp_history')
        .returning('id')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "110",
            measured_diastolic_blood_pressure: "90",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",

        })
        .into('bp_history')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "120",
            measured_diastolic_blood_pressure: "100",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",

        })
        .into('bp_history')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "130",
            measured_diastolic_blood_pressure: "110",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
            followed_by: adminId,
        })
        .into('bp_history')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "130",
            measured_diastolic_blood_pressure: "110",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
            followed_by: adminId,
        })
        .into('bp_history')

    await knex
        .insert({
            users_id: userID,
            measured_systolic_blood_pressure: "130",
            measured_diastolic_blood_pressure: "110",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
            followed_by: adminId,
        })
        .into('bp_history')

    //user2
    const [userID2] =
        await knex
            .insert({
                usercode: "12345678",
                chinese_name: "陳小文",
                english_name: "Chan Siu Man",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID2,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",

        })
        .into('bp_history')
        .returning('id')



    //user3
    const [userID3] =
        await knex
            .insert({
                usercode: "87654321",
                chinese_name: "陳大明",
                english_name: "Chan Tai Ming",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID3,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')

    //user4
    const [userID4] =
        await knex
            .insert({
                usercode: "43256789",
                chinese_name: "陳家明",
                english_name: "Chan Ka Ming",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID4,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')

    //user5
    const [userID5] =
        await knex
            .insert({
                usercode: "02489759",
                chinese_name: "陳嘉明",
                english_name: "Chan Ka Ming",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID5,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')


    //user6
    const [userID6] =
        await knex
            .insert({
                usercode: "09123456",
                chinese_name: "陳家文",
                english_name: "Chan Ka Man",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID6,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')


    //user7
    const [userID7] =
        await knex
            .insert({
                usercode: "3232143",
                chinese_name: "陳大文7",
                english_name: "Chan Tai Man7",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID7,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')

    //user8
    const [userID8] =
        await knex
            .insert({
                usercode: "43214321",
                chinese_name: "陳大文8",
                english_name: "Chan Tai Man8",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID8,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')

    //user9
    const [userID9] =
        await knex
            .insert({
                usercode: "98763456",
                chinese_name: "陳大文9",
                english_name: "Chan Tai Man9",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID9,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')

    //user10
    const [userID10] =
        await knex
            .insert({
                usercode: "12121212",
                chinese_name: "陳大文10",
                english_name: "Chan Tai Man10",
                home_number: "23984723",
                contact_person: "陳一一",
                contact_person_number: "23800000",
                emergency_contact: "123",
                emergency_contact_number: "09876543",
                gender: '男',
                default_systolic_blood_pressure_top_level: "150",
                default_systolic_blood_pressure_low_level: "80",
                default_diastolic_blood_pressure_top_level: "120",
                default_diastolic_blood_pressure_low_level: "50",
                default_blood_oxygen_contact_top_level: "100",
                default_blood_oxygen_contact_low_level: "50",
                default_heart_rate_top_level: "100",
                default_heart_rate_low_level: "50",
            })
            .into("normal_users")
            .returning('id');

    await knex
        .insert({
            users_id: userID10,
            measured_systolic_blood_pressure: "100",
            measured_diastolic_blood_pressure: "80",
            measured_blood_oxygen_contact: "95",
            measured_heart_rate: "120",
        })
        .into('bp_history')



};

