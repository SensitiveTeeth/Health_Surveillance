import React, { useEffect, } from 'react';
import './App.scss';
import './pages/LoginPage.scss'
import './pages/AddUserPage.scss'
import './pages/FollowUpPage.scss'
import './pages/AddUserPage.scss'
import './pages/UserDetailPage.scss'
import { Switch, Route } from 'react-router-dom'
import FollowUpPage from './pages/FollowUpPage';
import LoginPage from './pages/LoginPage'
import AddUserPage from './pages/AddUserPage';
import AllUserPage from './pages/AllUserPage';
import ColleaguePage from './pages/ColleaguePage'
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { restoreLogin } from './redux/auth/actions';
import { PrivateRoute } from './PrivateRoute';
import AddColleaguePage from './pages/AddColleaguePage';
import UserDetailPage from './pages/UserDetailPage';



function App() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)

  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(restoreLogin())
  }, [dispatch])



  return (
    <div className="App">
        {isAuthenticated === null ? <div>Loading...</div> :
          <Switch>
            <PrivateRoute path="/add-user"> <AddUserPage /></PrivateRoute>
            <PrivateRoute path="/add-colleague"> <AddColleaguePage /></PrivateRoute>
            <PrivateRoute path="/user-manage"> <AllUserPage /></PrivateRoute>
            <PrivateRoute path="/colleague"> <ColleaguePage /></PrivateRoute>
            <PrivateRoute path="/followup"> <FollowUpPage /> </PrivateRoute>
            <PrivateRoute path="/user-personal/:id"> <UserDetailPage /> </PrivateRoute>

            <Route path="/" ><LoginPage /></Route>
          </Switch>}
    </div>
  );
}

export default App;
