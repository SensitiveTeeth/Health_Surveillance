import React, { useEffect, useState } from 'react';
import { Container, Table, ListGroup, ListGroupItem, Row, Nav, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { perPage } from '../redux/followup/reducer';
import { logout } from '../redux/auth/actions';
import { UserPage, fetchUserPage } from '../redux/userManage/actions';
function AllUserPage() {
    // const normal_userIds = useSelector((state: RootState) => state.normal_user.normal_userIds)
    // const normal_users = useSelector((state: RootState) => normal_userIds.map(id => state.normal_user.normal_userById[id]))
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)
    const userPageIds = useSelector((state: RootState) => state.user_page.userPageIds)
    const userPages = useSelector((state: RootState) => userPageIds.map(id => state.user_page.userPageById[id]))
    const total = useSelector((state: RootState) => state.user_page.userPageCount)
    useEffect(() => {
        dispatch(fetchUserPage(page))
    }, [page, dispatch])




    const renderUserPage = (user_page: UserPage) => (
        <tr key={user_page.id}>
            <th><Button variant="info" href={`user-personal/${user_page.id}`} >更多</Button></th>
            <th>{user_page.chinese_name}</th>
            <th>{user_page.english_name}</th>
            <th>{user_page.gender}</th>
        </tr>
    )
    return (
        <div className="adminDivHolder">
            <div>
                <ListGroup>
                    <ListGroupItem>
                        <Row>
                            <Nav defaultActiveKey="/home" as="ul">
                            <Nav.Item as="li">
                                <Nav.Link href="/followup">跟進管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/colleague">同事管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-user">新增使用者</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                            </Nav.Item>

                            </Nav>
                        </Row>
                    </ListGroupItem>
                    <ListGroupItem>
                        <Container className="adminDiv" fluid="md">
                            <Row className="followUpPageRow">

                                <div className="upperFollowUpContainer">
                                    <Container className="upperFollowUpContainer">
                                        <div>
                                            <h3> </h3>
                                        </div>
                                    </Container>
                                    <Container className="followUpContainer">
                                        <div>
                                            <h2>使用者管理</h2>
                                        </div>
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th>更多資料</th>
                                                <th>中文姓名</th>
                                                <th>英文姓名</th>
                                                <th>性別</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            {userPages.slice((page - 1) * perPage, page * perPage).map(renderUserPage)}
                                        </thead>

                                    </Table>
                                    {page > 1 && <Button className="pageButton" variant="outline-secondary" size="sm" onClick={() => setPage(page - 1)}>Prev Page</Button>}
                                    {page < Math.ceil(total / perPage) && <Button variant="outline-secondary" size="sm" onClick={() => setPage(page + 1)}>Next Page</Button>}
                                </div >
                            </Row>
                        </Container>
                    </ListGroupItem>
                </ListGroup>
            </div>
        </div>
    )
}
export default AllUserPage;