import React, { useState } from 'react'
import { useFormState } from 'react-use-form-state'
import { Container, Col, ListGroup, ListGroupItem, Row, Nav, Button, Form } from 'react-bootstrap';
import { logout } from '../redux/auth/actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';

export function AddColleaguePage() {
    const dispatch = useDispatch();
    const [errorMessage, setErrorMessage] = useState<string | null>(null)
    const [error, setError] = useState<boolean | null>(false)
    const [formState, { text }] = useFormState();
    const token = useSelector((state: RootState) => state.auth.token)

    return (
        <div className="adminDivHolder">
            <ListGroup>
                <ListGroupItem>
                    <Row>
                        <Nav defaultActiveKey="/home" as="ul">
                            <Nav.Item as="li">
                                <Nav.Link href="/followup">跟進管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/colleague">同事管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-user">新增使用者</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Row>
                </ListGroupItem>
                <ListGroupItem>
                    <Container className="adminDiv" fluid="md">
                        <Row className="followUpPageRow" >
                            <div className="upperFollowUpContainer">
                                <Container className="upperFollowUpContainer">
                                </Container>
                                <Container className="followUpContainer">
                                    <div>
                                        <h2>新增同事</h2>
                                    </div>
                                </Container>
                                <Container className="blankContainer">
                                </Container>
                                <Container className="blankContainer">
                                </Container>
                                <form className="form" onSubmit={async event => {
                                    event.preventDefault();
                                    try {
                                        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/admin`, {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'Authorization': `Bearer ${token}`
                                            },
                                            body: JSON.stringify(formState.values)
                                        })
                                        const json = await res.json();
                                        if (json.message) {
                                            setErrorMessage(json.message)
                                            setError(true)
                                        }
                                        if (res.status === 200) {
                                            formState.reset()
                                        }
                                    } catch (e) {
                                        console.error(e);
                                    }
                                }}>
                                    <Container className="inputContainer">
                                        <div>
                                            {error && <p>{errorMessage}</p>}
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridUserCode">
                                                    <Form.Label>登入號碼</Form.Label>
                                                    <Form.Control {...text('usercode')} type="text" placeholder="請輸入號碼" maxLength={8} />
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridPassword">
                                                    <Form.Label>密碼</Form.Label>
                                                    <Form.Control {...text('password')} type="password" placeholder="請輸入密碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridUsername">
                                                    <Form.Label>稱謂</Form.Label>
                                                    <Form.Control {...text('username')} type="text" placeholder="請輸入稱謂" />
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridPosition">
                                                    <Form.Label>職位</Form.Label>
                                                    <Form.Control {...text('position')} type="text" placeholder="請輸入職位" />
                                                </Form.Group>
                                            </Form.Row>
                                        </div>
                                    </Container>
                                    <Button variant="success" type="submit" >增加新同事</Button>
                                </form>
                            </div >
                        </Row>
                    </Container>
                </ListGroupItem>
            </ListGroup>
        </div >
    )
}
export default AddColleaguePage;