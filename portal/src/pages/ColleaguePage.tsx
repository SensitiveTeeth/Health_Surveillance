import React, { useEffect, useState } from 'react';
import { Container, Table, ListGroup, ListGroupItem, Row, Nav, Button, Modal, Form, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetch_Colleague, Colleague } from '../redux/colleague/actions'
import { perPage } from '../redux/followup/reducer';
import { logout } from '../redux/auth/actions';
import { useFormState } from 'react-use-form-state';
import { RootState } from '../store';

function ColleaguePage() {
    const [editing, setEditing] = useState<number | null>(null);
    const [errorMessage, setErrorMessage] = useState<number | null>(null)
    const [error, setError] = useState<boolean | null>(false)
    const [formState, { text }] = useFormState();
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)
    const colleagueIds = useSelector((state: RootState) => state.colleague.colleagueIds)
    const colleagues = useSelector((state: RootState) => colleagueIds.map(id => state.colleague.colleagueById[id]))
    const total = useSelector((state: RootState) => state.colleague.colleagueCount)
    const token = useSelector((state: RootState) => state.auth.token)

    useEffect(() => {
        dispatch(fetch_Colleague(page))
    }, [page, dispatch])


    const showModal = (colleague: Colleague) => {
        setEditing(colleague.id);
        formState.setField('usercode', colleague.usercode);
        formState.setField('username', colleague.username);
        formState.setField('position', colleague.position);

    }

    const renderColleague = (colleague: Colleague) => (
        <tr key={colleague.id}>
            <th><Button onClick={() => showModal(colleague)} >更改</Button></th>
            <th>{colleague.username}</th>
            <th>{colleague.position}</th>
            <th>{colleague.usercode}</th>
        </tr>
    )

    return (
        <div className="adminDivHolder">
            <div>
                <ListGroup>
                    <ListGroupItem>
                        <Row>
                            <Nav defaultActiveKey="/home" as="ul">
                                <Nav.Item as="li">
                                    <Nav.Link href="/followup">跟進管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/colleague">同事管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-user">新增使用者</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Row>
                    </ListGroupItem>
                    <ListGroupItem>
                        <Container className="adminDiv" fluid="md">
                            <Row className="followUpPageRow">

                                <div className="upperFollowUpContainer">
                                    <Container className="upperFollowUpContainer">
                                        <div>
                                            <h3> </h3>
                                        </div>
                                    </Container>
                                    <Container className="followUpContainer">
                                        <div>
                                            <h2>同事管理</h2>
                                        </div>
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th>更改資料</th>
                                                <th>姓名</th>
                                                <th>角色</th>
                                                <th>電話</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            {colleagues.slice((page - 1) * perPage, page * perPage).map(renderColleague)}
                                        </thead>
                                    </Table>
                                    {page > 1 && <Button variant="outline-secondary" size="sm" onClick={() => setPage(page - 1)}>Prev Page</Button>}
                                    {page < Math.ceil(total / perPage) && <Button variant="outline-secondary" size="sm" onClick={() => setPage(page + 1)}>Next Page</Button>}
                                </div >
                            </Row>
                        </Container>

                    </ListGroupItem>
                </ListGroup>
                <Modal show={editing !== null} onHide={() => setEditing(null)}>
                    <form className="form" onSubmit={async event => {
                        event.preventDefault();
                        try {
                            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/admin/${editing}`, {
                                method: 'PUT',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${token}`
                                },
                                body: JSON.stringify(formState.values)
                            })
                            const json = await res.json();
                            if (json.message) {
                                setErrorMessage(json.message)
                                setError(true)
                            }
                            if (res.status === 200) {
                                // formState.reset()
                                setEditing(null)
                                window.location.reload();
                            }
                        } catch (e) {
                            console.error(e);
                        }
                    }}>
                        <Modal.Header closeButton>
                            <Modal.Title>更改 {formState.values.username} 的資料</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {error && <p>{errorMessage}</p>}
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridUserCode">
                                    <Form.Label>新登入號碼</Form.Label>
                                    <Form.Control {...text('usercode')} type="text" maxLength={8} value={(formState.values.usercode)} />
                                </Form.Group>

                                <Form.Group as={Col} controlId="formGridPassword">
                                    <Form.Label>新密碼</Form.Label>
                                    <Form.Control {...text('password')} type="password" maxLength={8} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridUsername">
                                    <Form.Label>稱謂</Form.Label>
                                    <Form.Control {...text('username')} type="text" value={formState.values.username} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridPosition">
                                    <Form.Label>職位</Form.Label>
                                    <Form.Control {...text('position')} type="text" value={formState.values.position} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Group as={Col} controlId="formGridOldPassword">
                                <Form.Label>舊有密碼</Form.Label>
                                <Form.Control {...text('oldPassword')} type="password" maxLength={8} />
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={() => setEditing(null)}>
                                取消
                        </Button>
                            <Button variant="primary" type="submit" >
                                儲存
                        </Button>
                        </Modal.Footer>
                    </form>
                </Modal>
            </div>
        </div>

    )

}

export default ColleaguePage;