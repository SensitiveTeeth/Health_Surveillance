import React, { useEffect, useState } from 'react';
import { Container, Col, Table, ListGroup, ListGroupItem, Row, Nav, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchFollowup, Followup } from '../redux/followup/actions'
import { RootState } from '../store';
import Moment from 'react-moment'
import { perPage } from '../redux/followup/reducer';
import { logout } from '../redux/auth/actions';

function FollowUpPage() {
    // const normal_userIds = useSelector((state: RootState) => state.normal_user.normal_userIds)
    // const normal_users = useSelector((state: RootState) => normal_userIds.map(id => state.normal_user.normal_userById[id]))
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)
    const followupIds = useSelector((state: RootState) => state.followup.followupIds)
    const Followups = useSelector((state: RootState) => followupIds.map(id => state.followup.followupById[id]))
    const total = useSelector((state: RootState) => state.followup.followupCount)
    useEffect(() => {
        dispatch(fetchFollowup(page))
    }, [page, dispatch])




    const renderFollowup = (followup: Followup) => (
        <tr key={followup.id}>
            <th>{followup.chinese_name}</th>
            <th>{followup.measured_systolic_blood_pressure}</th>
            <th>{followup.measured_diastolic_blood_pressure}</th>
            <th>{followup.measured_blood_oxygen_contact}</th>
            <th>{followup.measured_heart_rate}</th>
            <th><Moment format="DD/MM HH:mm">{followup.date}</Moment></th>
        </tr>
    )
    return (
        <div className="adminDivHolder">
            <ListGroup>
                <ListGroupItem>
                    <Row>
                        <Nav>
                            <Nav.Item as="li">
                                <Nav.Link href="/followup">跟進管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/colleague">同事管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-user">新增使用者</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Row>
                </ListGroupItem>
                <ListGroupItem className="listGroupItem2">
                    <Container className="adminDiv" fluid="md">
                        <Row className="followUpPageRow">

                            <div className="upperFollowUpContainer">
                                <Container className="upperFollowUpContainer">
                                    <div>
                                        <h3> </h3>
                                    </div>
                                </Container>
                                <Container className="followUpContainer">
                                    <div>
                                        <h2>跟進管理</h2>
                                    </div>
                                </Container>
                                <Container className="blankContainer">
                                </Container>
                                <Container className="threeDivHolder">
                                    <div className="needToFollowDiv roundCorner">
                                        <Col>
                                            <h2>0</h2>
                                        </Col>
                                        <Col>
                                            需要跟進
                                            </Col>
                                    </div>
                                    <div className="divBetweenDiv">
                                    </div>
                                    <div className="needToMeasureDiv roundCorner">
                                        <Col>
                                            <h2>2</h2>
                                        </Col>
                                        <Col>
                                            未量血壓
                                        </Col>
                                    </div>
                                    <div className="divBetweenDiv">
                                    </div>
                                    <div className="alreadyMeasuredDiv roundCorner">
                                        <Col >
                                            <h2>
                                                5
                                            </h2>
                                        </Col>
                                        <Col>
                                            已量血壓
                                        </Col>
                                    </div>
                                </Container>
                                <Container className="blankContainer">
                                </Container>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>姓名</th>
                                            <th>上壓</th>
                                            <th>下壓</th>
                                            <th>血氧</th>
                                            <th>心跳</th>
                                            <th>量度時間</th>
                                        </tr>
                                    </thead>
                                    <thead>
                                        {Followups.slice((page - 1) * perPage, page * perPage).map(renderFollowup)}
                                    </thead>

                                </Table>
                                {page > 1 && <Button variant="outline-secondary" size="sm" onClick={() => setPage(page - 1)}>Prev Page</Button>}
                                {page < Math.ceil(total / perPage) && <Button variant="outline-secondary" size="sm" onClick={() => setPage(page + 1)}>Next Page</Button>}
                            </div >
                        </Row>
                    </Container>

                </ListGroupItem>
            </ListGroup>
        </div>





    )

}


export default FollowUpPage;