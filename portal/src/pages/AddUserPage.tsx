import React, { useState, useRef } from 'react'
import { useFormState } from 'react-use-form-state'
import { Container, Col, ListGroup, ListGroupItem, Row, Nav, Button, Form } from 'react-bootstrap';
import { logout } from '../redux/auth/actions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';

export function AddUserPage() {
    const backToTop = useRef<any>();
    const dispatch = useDispatch();
    const [Message, setMessage] = useState<string | null>(null)
    const [error, setError] = useState<boolean | null>(false)
    const [formState, { text, radio }] = useFormState();
    const token = useSelector((state: RootState) => state.auth.token)

    return (
        <div className="adminDivHolder">
            <form className="form" onSubmit={async event => {
                event.preventDefault();
                const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(formState.values)
                })
                const json = await res.json();
                if (json.message) {
                    setMessage(json.message)
                    setError(true)
                }
                if (res.status === 200) {
                    formState.reset()
                }

            }}>
                <ListGroup>
                    <ListGroupItem>
                        <Row>
                            <Nav defaultActiveKey="/home" as="ul">
                                <Nav.Item as="li">
                                    <Nav.Link href="/followup">跟進管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/colleague">同事管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-user">新增使用者</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Row>
                    </ListGroupItem>
                    <ListGroupItem>
                        <Container className="adminDiv" fluid="md" ref={backToTop}>
                            <Row className="followUpPageRow">
                                <div className="upperFollowUpContainer">
                                    <Container className="upperFollowUpContainer">
                                    </Container>
                                    <Container className="followUpContainer">
                                        <div>
                                            <h2>新增使用者</h2>
                                        </div>
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Container className="inputContainer">
                                        <div>
                                            {error && <p>{Message}</p>}{error && backToTop.current.scrollTo(0, 0)}
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridUserCode">
                                                    <Form.Label>登入號碼</Form.Label>
                                                    <Form.Control {...text('usercode')} type="text" placeholder="請輸入號碼" maxLength={8} />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridHome_number">
                                                    <Form.Label>家居號碼</Form.Label>
                                                    <Form.Control {...text('home_number')} type="text" placeholder="請輸入家居號碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridChinese_name">
                                                    <Form.Label>中文名稱</Form.Label>
                                                    <Form.Control {...text('chinese_name')} type="text" placeholder="請輸入中文名稱" />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridEnglish_name">
                                                    <Form.Label>英文名稱</Form.Label>
                                                    <Form.Control {...text('english_name')} type="text" placeholder="請輸入英文名稱" />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridContact_person">
                                                    <Form.Label>聯絡人</Form.Label>
                                                    <Form.Control {...text('contact_person')} type="text" placeholder="請輸入聯絡人名稱" />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridChinese_name">
                                                    <Form.Label>聯絡人號碼</Form.Label>
                                                    <Form.Control {...text('contact_person_number')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>緊急聯絡人</Form.Label>
                                                    <Form.Control {...text('emergency_contact')} type="text" placeholder="請輸入聯絡人名稱" />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number">
                                                    <Form.Label>緊急聯絡人號碼</Form.Label>
                                                    <Form.Control {...text('emergency_contact_number')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>緊急聯絡人(2)</Form.Label>
                                                    <Form.Control {...text('emergency_contact2')} type="text" placeholder="請輸入聯絡人名稱" />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                    <Form.Label>緊急聯絡人號碼(2)</Form.Label>
                                                    <Form.Control {...text('emergency_contact_number2')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>緊急聯絡人(3)</Form.Label>
                                                    <Form.Control {...text('emergency_contact3')} type="text" placeholder="請輸入聯絡人名稱" />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number3">
                                                    <Form.Label>緊急聯絡人號碼(3)</Form.Label>
                                                    <Form.Control {...text('emergency_contact_number3')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>覆診醫生</Form.Label>
                                                    <Form.Control {...text('doctor_in_charge')} type="text" placeholder="請輸入覆診醫生名稱" />
                                                </Form.Group>
                                                <fieldset className="genderField">
                                                    <Form.Group as={Row}>
                                                        <Form.Label as="legend" column sm={10}>
                                                            性別
                                                        </Form.Label>
                                                        <Col sm={10}>
                                                            <input {...radio('gender', '男')} type="radio" value="男" />男
                                                            <div></div>
                                                            <input {...radio('gender', '女')} type="radio" value='女' />女
                                                        </Col>
                                                    </Form.Group>
                                                </fieldset>
                                            </Form.Row>
                                            {/* <Form.Row className="uploadFileRow">
                                                <fieldset>
                                                    <Form.Group>
                                                        <Form.File className="uploadFileForm" id="formcheck-api-regular">
                                                            <Form.File.Label>頭像</Form.File.Label>
                                                            <Form.File.Input />
                                                        </Form.File>
                                                    </Form.Group>
                                                </fieldset>
                                            </Form.Row> */}
                                            <Form.Row>
                                                <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                    <Form.Label >藥物敏感</Form.Label>
                                                    <Form.Control {...text('drug_allergy')} as="textarea" rows={15} />
                                                </Form.Group>
                                                <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                    <Form.Label>食物敏感</Form.Label>
                                                    <Form.Control {...text('food_allergy')} as="textarea" rows={15} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group className="textInputArea" controlId="exampleForm.ContolTextarea1">
                                                    <Form.Label>服用藥物</Form.Label>
                                                    <Form.Control {...text('drug_history')} as="textarea" rows={15} />
                                                </Form.Group>
                                                <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                    <Form.Label>病歷資料</Form.Label>
                                                    <Form.Control {...text('medical_history')} as="textarea" rows={15} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group className="textInputArea" controlId="exampleForm.ContolTextarea1">
                                                    <Form.Label>住院紀錄</Form.Label>
                                                    <Form.Control {...text('hospital_history')} as="textarea" rows={15} />
                                                </Form.Group>

                                            </Form.Row>

                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>血壓上壓上限</Form.Label>
                                                    <Form.Control {...text('default_systolic_blood_pressure_top_level')} type="text" placeholder="請輸入血壓上壓上限" maxLength={3} />
                                                </Form.Group>

                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                    <Form.Label>血壓上壓下限</Form.Label>
                                                    <Form.Control {...text('default_systolic_blood_pressure_low_level')} type="text" placeholder="請輸入血壓上壓下限" maxLength={3} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>血壓下壓上限</Form.Label>
                                                    <Form.Control {...text('default_diastolic_blood_pressure_top_level')} type="text" placeholder="請輸入血壓下壓上限" maxLength={3} />
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                    <Form.Label>血壓下壓下限</Form.Label>
                                                    <Form.Control {...text('default_diastolic_blood_pressure_low_level')} type="text" placeholder="請輸入血壓下壓下限" maxLength={3} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>血氧上限</Form.Label>
                                                    <Form.Control {...text('default_blood_oxygen_contact_top_level')} type="text" placeholder="請輸入血氧上限" maxLength={3} />
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                    <Form.Label>血氧下限</Form.Label>
                                                    <Form.Control {...text('default_blood_oxygen_contact_low_level')} type="text" placeholder="請輸入血氧下限" maxLength={3} />
                                                </Form.Group>
                                            </Form.Row>
                                            <Form.Row>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                    <Form.Label>心率上限</Form.Label>
                                                    <Form.Control {...text('default_heart_rate_top_level')} type="text" placeholder="請輸入心率上限" maxLength={3} />
                                                </Form.Group>
                                                <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                    <Form.Label>心率下限</Form.Label>
                                                    <Form.Control {...text('default_heart_rate_low_level')} type="text" placeholder="請輸入心率下限" maxLength={3} />
                                                </Form.Group>
                                            </Form.Row>
                                        </div>
                                    </Container>
                                    <Button variant="success" type="submit">增加新使用者</Button>
                                </div >
                            </Row>
                        </Container>
                    </ListGroupItem>
                </ListGroup>
            </form>
        </div>
    )
}
export default AddUserPage;