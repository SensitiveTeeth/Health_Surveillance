import React, { useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useFormState } from 'react-use-form-state';
import { useDispatch, useSelector } from 'react-redux';
import { login, clearError } from '../redux/auth/actions';
import { RootState } from '../store';

function LoginPage() {
    const [formState, { text, password }] = useFormState();
    const dispatch = useDispatch();
    const errorMessage = useSelector((state:RootState) => state.auth.error)

    useEffect(() =>{
        return () =>{
            dispatch(clearError())
        }
    },[dispatch])
    return (
        <Container fluid="md" className="loginDivHolder">

            <div className="loginDiv" >
                <h2>系統登入</h2>
                {errorMessage ? <p>{errorMessage}</p>:<p></p>}
                <form onSubmit={event => {
                    event.preventDefault();

                    dispatch(login(formState.values.usercode, formState.values.password))
                }}>
                    <Container fluid="md">
                        <Row>
                            <Col><p><input placeholder="Usercode"{...text('usercode')} /></p></Col>
                        </Row>
                        <Row>
                            <Col><p><input placeholder="Password"{...password('password')} /></p></Col>
                        </Row>
                        <Row>
                            <Col>
                                <button className="loginButton" type = "submit">登入</button>
                            </Col>
                        </Row>
                    </Container>
                </form>
            </div>
        </Container>
    );
}
export default LoginPage;