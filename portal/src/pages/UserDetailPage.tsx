import React, { useEffect, useState } from 'react';
import { Container, ListGroup, ListGroupItem, Row, Nav, Button, Modal, Form, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchBpHistory, BpHistory } from '../redux/bp_history/actions'
import { RootState } from '../store';
import useReactRouter from 'use-react-router';
import Moment from 'react-moment'
import { perPage } from '../redux/bp_history/reducer';
import { logout } from '../redux/auth/actions';
import { useFormState } from 'react-use-form-state';
import { Normal_User, initialNormalUser, initialBpHistory } from '../model'
import { fetchRemark } from '../redux/remark/actions';



function UserDetailPage() {
    const [Message, setMessage] = useState<string | null>(null)
    const [error, setError] = useState<boolean | null>(false)
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)

    const [BpSingleHistory, setBpSingleHistory] = useState<BpHistory>(initialBpHistory)
    const [userDetail, setUserDetail] = useState<Normal_User>(initialNormalUser)
    const bpHistoryIds = useSelector((state: RootState) => state.bp_history.bpHistoryIds)
    const BpHistories = useSelector((state: RootState) => bpHistoryIds.map(id => state.bp_history.bpHistoryById[id]))
    const remark = useSelector((state: RootState) => state.remark)
    const { match } = useReactRouter<{ id: string }>()
    const total = useSelector((state: RootState) => state.bp_history.bpHistoryCount)
    const [editing, setEditing] = useState<number | null>(null);
    const [formState, { text, radio }] = useFormState();
    const [remarkFormState, { text:text2 } ] = useFormState();

    const token = useSelector((state: RootState) => state.auth.token);
    const userId = parseInt(match.params.id)

    function fetchLatestBp() {
        return async () => {
            try {
                const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/bp_history/${match.params.id}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                })
                const json = await res.json();
                if (res.status === 200) {
                    setBpSingleHistory(json.bp_history[0])
                    dispatch(fetchRemark(json.bp_history[0].id))

                }
            } catch (e) {
                console.error(e)
            }
        }
    }
    function fetchNormalUser() {
        return async () => {
            try {
                const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/${match.params.id}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                })
                const json = await res.json();
                if (res.status === 200) {
                    // await fetchRemarkByBpHistoryId()
                    setUserDetail(json[0])

                }
            } catch (e) {
                console.error(e)

            }
        }
    }

    useEffect(() => {
        dispatch(fetchBpHistory(page, userId))
    }, [page, userId, dispatch])
// eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        dispatch(fetchNormalUser())
        // eslint-disable-next-line react-hooks/exhaustive-deps
        dispatch(fetchLatestBp())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch])


    const showModal = (userDetail: Normal_User) => {
        setEditing(userDetail.id);
        formState.setField('chinese_name', userDetail.chinese_name);
        formState.setField('usercode', userDetail.usercode);
        formState.setField('chinese_name', userDetail.chinese_name);
        formState.setField('english_name', userDetail.english_name);
        formState.setField('home_number', userDetail.home_number);
        formState.setField('contact_person', userDetail.contact_person);
        formState.setField('contact_person_number', userDetail.contact_person_number);
        formState.setField('emergency_contact', userDetail.emergency_contact);
        formState.setField('emergency_contact_number', userDetail.emergency_contact_number);
        formState.setField('emergency_contact2', userDetail.emergency_contact2);
        formState.setField('emergency_contact_number2', userDetail.emergency_contact_number2);
        formState.setField('emergency_contact3', userDetail.emergency_contact3);
        formState.setField('emergency_contact_number3', userDetail.emergency_contact_number3);
        formState.setField('gender', userDetail.gender);
        formState.setField('doctor_in_charge', userDetail.doctor_in_charge);
        formState.setField('drug_allergy', userDetail.drug_allergy);
        formState.setField('food_allergy', userDetail.food_allergy);
        formState.setField('drug_history', userDetail.drug_history);
        formState.setField('medical_history', userDetail.medical_history);
        formState.setField('hospital_history', userDetail.hospital_history);
        formState.setField('default_systolic_blood_pressure_top_level', userDetail.default_systolic_blood_pressure_top_level);
        formState.setField('default_systolic_blood_pressure_low_level', userDetail.default_systolic_blood_pressure_low_level);
        formState.setField('default_diastolic_blood_pressure_top_level', userDetail.default_diastolic_blood_pressure_top_level);
        formState.setField('default_diastolic_blood_pressure_low_level', userDetail.default_diastolic_blood_pressure_low_level);
        formState.setField('default_blood_oxygen_contact_top_level', userDetail.default_blood_oxygen_contact_top_level);
        formState.setField('default_blood_oxygen_contact_low_level', userDetail.default_blood_oxygen_contact_low_level);
        formState.setField('default_heart_rate_top_level', userDetail.default_heart_rate_top_level);
        formState.setField('default_heart_rate_low_level', userDetail.default_heart_rate_low_level);
    }


    const renderBpHistory = (bp_history: BpHistory) => (
        <Row className="bpHistoryRenderRow" key={bp_history.id}>
            <Col sm={2}><Button variant="info" onClick={() => {
                setBpSingleHistory(bp_history)
                dispatch(fetchRemark(bp_history.id))
            }}>更多</Button></Col>
            <Col sm={2}>{bp_history.measured_systolic_blood_pressure}</Col>
            <Col sm={2}>{bp_history.measured_diastolic_blood_pressure}</Col>
            <Col sm={2}>{bp_history.measured_blood_oxygen_contact}</Col>
            <Col sm={2}>{bp_history.measured_heart_rate}</Col>
            <Col sm={2}><Moment format="MM月DD日 HH:mm">{bp_history.created_at}</Moment></Col>
        </Row>
    )
    return (
        <div className="adminDivHolder">
            <div>
                <ListGroup>
                    <ListGroupItem>
                        <Row>
                            <Nav defaultActiveKey="/home" as="ul">
                                <Nav.Item as="li">
                                    <Nav.Link href="/followup">跟進管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/colleague">同事管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/user-manage">使用者管理</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-user">新增使用者</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link href="/add-colleague">新增同事</Nav.Link>
                                </Nav.Item>
                                <Nav.Item as="li">
                                    <Nav.Link onClick={() => dispatch(logout())}>登出</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Row>
                    </ListGroupItem>
                    <ListGroupItem>
                        <Container className="adminDiv" fluid="md">
                            <Row className="followUpPageRow">
                                <div className="upperFollowUpContainer">
                                    <Container className="upperFollowUpContainer">
                                    </Container>
                                    <Container className="followUpContainer">
                                        <div>
                                            <h2>跟進管理: {userDetail.chinese_name}</h2>
                                        </div>
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Container className="blankContainer">
                                    </Container>
                                    <Container>
                                        <Row>
                                            <Col sm={7}>
                                                <Container >
                                                    <Row>
                                                        <Container className="userContainer">
                                                            <Col>
                                                                <Row>
                                                                    <Col>量度時間</Col>
                                                                    <Col sm={6}>
                                                                        {BpSingleHistory.created_at !== "" && (<Moment format="MM月DD日 HH:mm">{BpSingleHistory.created_at}</Moment>)}
                                                                    </Col>
                                                                </Row>
                                                                <Row >
                                                                    <Col sm={3}>上壓</Col>
                                                                    <Col sm={3}>下壓</Col>
                                                                    <Col sm={3}>血氧</Col>
                                                                    <Col sm={3}>心跳</Col>
                                                                </Row>
                                                                <Row >
                                                                    <Col sm={3}>{BpSingleHistory.measured_systolic_blood_pressure}</Col>
                                                                    <Col sm={3}>{BpSingleHistory.measured_diastolic_blood_pressure}</Col>
                                                                    <Col sm={3}>{BpSingleHistory.measured_blood_oxygen_contact}</Col>
                                                                    <Col sm={3}>{BpSingleHistory.measured_heart_rate}</Col>
                                                                </Row>
                                                                <Row className="betweenRow">
                                                                </Row>
                                                                {remark.remarkIds.map((singleRemark) => {
                                                                    return (
                                                                        <Row key={singleRemark.id} sm={12}>
                                                                            <Col>
                                                                                備註:{singleRemark.text_remark}
                                                                            </Col>
                                                                            <Col>
                                                                                <Moment format="MM月DD日 HH:mm">{singleRemark.created_at}</Moment>
                                                                            </Col>
                                                                        </Row>
                                                                    )
                                                                })}
                                                                <Row className="betweenRow">
                                                                </Row>
                                                                <form className="form" onSubmit={async event => {
                                                                    event.preventDefault();
                                                                    try {
                                                                        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/remark/${BpSingleHistory.id}`, {
                                                                            method: 'POST',
                                                                            headers: {
                                                                                'Content-Type': 'application/json',
                                                                                'Authorization': `Bearer ${token}`
                                                                            },
                                                                            body: JSON.stringify(remarkFormState.values)
                                                                        })
                                                                        // const json = await res.json();
                                                                        // console.log(remarkFormState)

                                                                        if (res.status === 200) {
                                                                            remarkFormState.reset()
                                                                            dispatch(fetchRemark(BpSingleHistory.id))
                                                                        }
                                                                    } catch (e) {
                                                                        console.error(e)
                                                                    }
                                                                }}>
                                                                    <Row>
                                                                        <Col sm={9}>
                                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                                <Form.Control {...text2('remark')} as="textarea" rows={3}></Form.Control>
                                                                            </Form.Group>
                                                                        </Col>
                                                                        <Col sm={3}><Button type="submit">新增備註</Button></Col>
                                                                    </Row>
                                                                </form>
                                                            </Col>
                                                        </Container>
                                                    </Row>
                                                    <Row className="blankBetweenRow">
                                                    </Row>
                                                    <Row >
                                                        <Container className="smallerFont userContainer">
                                                            <Col>
                                                                <Row>
                                                                    <Col sm={2}>更多資料</Col>
                                                                    <Col sm={2}>上壓</Col>
                                                                    <Col sm={2}>下壓</Col>
                                                                    <Col sm={2}>血氧</Col>
                                                                    <Col sm={2}>心跳</Col>
                                                                    <Col sm={2}>量度時間</Col>
                                                                </Row>
                                                                {BpHistories.slice((page - 1) * perPage, page * perPage).map(renderBpHistory)}
                                                                <Row>
                                                                    <Col>
                                                                        {page > 1 && <Button onClick={() => setPage(page - 1)}>Prev Page</Button>}
                                                                        {page < Math.ceil(total / perPage) && <Button onClick={() => setPage(page + 1)}>Next Page</Button>}
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                        </Container>
                                                    </Row>
                                                </Container>
                                            </Col>
                                            <Col className="userOuterCol" sm={5}>
                                                <Container className="userContainer">
                                                    <Container className="userInnerContainer">
                                                        <Row className="blankUpperRow">
                                                        </Row>
                                                        <Row>
                                                            <Col sm={9}>
                                                                <h3>個人資料</h3>
                                                            </Col>
                                                            <Col sm={3}>
                                                                <Button onClick={() => showModal(userDetail)}>更改</Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col sm={6}>
                                                                <Row className="userRow">中文名稱: </Row>
                                                                <Row>英文名稱: </Row>
                                                                <Row>性別:</Row>
                                                                <Row>手提電話:</Row>
                                                                <Row>家居電話:</Row>
                                                                <Row>聯絡人:</Row>
                                                                <Row>聯絡人號碼:</Row>
                                                                <Row>緊急聯絡人(1):</Row>
                                                                <Row>(1)號碼:</Row>
                                                                <Row>緊急聯絡人(2):</Row>
                                                                <Row>(2)號碼:</Row>
                                                                <Row>緊急聯絡人(3):</Row>
                                                                <Row>(3)號碼: </Row>
                                                                <Row>覆診醫生:</Row>
                                                            </Col>
                                                            <Col sm={6}>
                                                                <Row >
                                                                    {userDetail.chinese_name}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.english_name}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.gender}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.usercode}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.home_number}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.contact_person}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.contact_person_number}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact_number}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact2}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact_number2}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact3}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.emergency_contact_number3}
                                                                </Row>
                                                            </Col>
                                                        </Row>
                                                        <Row><h4>食物敏感:</h4></Row>
                                                        <Row>{userDetail.food_allergy}</Row>
                                                        <Row><h4>藥物敏感:</h4></Row>
                                                        <Row>{userDetail.drug_allergy}</Row>
                                                        <Row> <h4>服用藥物:</h4></Row>
                                                        <Row>{userDetail.drug_history} </Row>
                                                        <Row><h4>病歷資料:</h4></Row>
                                                        <Row>{userDetail.medical_history} </Row>
                                                        <Row><h4>住院紀錄: </h4> </Row>
                                                        <Row> {userDetail.hospital_history}</Row>
                                                        <Row><h4>健康設定:</h4> </Row>
                                                        <Row>
                                                            <Col>
                                                                <Row>上壓</Row>
                                                                <Row>下壓</Row>
                                                                <Row>血氧</Row>
                                                                <Row>心跳</Row>
                                                            </Col>
                                                            <Col>
                                                                <Row>
                                                                    {userDetail.default_systolic_blood_pressure_low_level} - {userDetail.default_systolic_blood_pressure_top_level}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.default_diastolic_blood_pressure_low_level} - {userDetail.default_diastolic_blood_pressure_top_level}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.default_blood_oxygen_contact_low_level} - {userDetail.default_blood_oxygen_contact_top_level}
                                                                </Row>
                                                                <Row>
                                                                    {userDetail.default_heart_rate_low_level} - {userDetail.default_heart_rate_top_level}
                                                                </Row>
                                                            </Col>
                                                        </Row>
                                                    </Container>
                                                </Container>
                                            </Col>
                                        </Row>
                                    </Container>

                                </div >
                            </Row>
                        </Container>
                        <Modal size="xl" className="editUserModal" show={editing !== null} onHide={() => setEditing(null)}>
                            <form className="form" onSubmit={async event => {
                                event.preventDefault();
                                try {
                                    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user/${userDetail.id}`, {
                                        method: 'PUT',
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'Authorization': `Bearer ${token}`
                                        },
                                        body: JSON.stringify(formState.values)
                                    })
                                    const json = await res.json();
                                    if (json.message) {
                                        setMessage(json.message)
                                        setError(true)
                                    }
                                    if (res.status === 200) {
                                        setEditing(null)
                                        formState.reset()
                                        dispatch(fetchNormalUser())
                                    }
                                } catch (e) {
                                    console.error(e)
                                }
                            }}>
                                <Modal.Header closeButton>
                                    <Modal.Title>更改 {userDetail.chinese_name} 的資料</Modal.Title>
                                </Modal.Header>
                                <ListGroupItem>
                                    <Container className="adminDiv userDiv" fluid="md">
                                        <Row className="followUpPageRow">
                                            <div className="upperFollowUpContainer">
                                                <Container className="upperFollowUpContainer">
                                                </Container>
                                                <Container className="followUpContainer">
                                                    <div>
                                                    </div>
                                                </Container>

                                                <Container className="inputContainer">
                                                    <div>
                                                        {error && <Col>{Message}</Col>}
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridUserCode">
                                                                <Form.Label>登入號碼</Form.Label>
                                                                <Form.Control {...text('usercode')} type="text" placeholder="請輸入號碼" maxLength={8} />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridHome_number">
                                                                <Form.Label>家居號碼</Form.Label>
                                                                <Form.Control {...text('home_number')} type="text" placeholder="請輸入家居號碼" maxLength={8} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridChinese_name">
                                                                <Form.Label>中文名稱</Form.Label>
                                                                <Form.Control {...text('chinese_name')} type="text" placeholder="請輸入中文名稱" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEnglish_name">
                                                                <Form.Label>英文名稱</Form.Label>
                                                                <Form.Control {...text('english_name')} type="text" placeholder="請輸入英文名稱" />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridContact_person">
                                                                <Form.Label>聯絡人</Form.Label>
                                                                <Form.Control {...text('contact_person')} type="text" placeholder="請輸入聯絡人名稱" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridChinese_name">
                                                                <Form.Label>聯絡人號碼</Form.Label>
                                                                <Form.Control {...text('contact_person_number')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>緊急聯絡人</Form.Label>
                                                                <Form.Control {...text('emergency_contact')} type="text" placeholder="請輸入聯絡人名稱" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number">
                                                                <Form.Label>緊急聯絡人號碼</Form.Label>
                                                                <Form.Control {...text('emergency_contact_number')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>緊急聯絡人(2)</Form.Label>
                                                                <Form.Control {...text('emergency_contact2')} type="text" placeholder="請輸入聯絡人名稱" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                                <Form.Label>緊急聯絡人號碼(2)</Form.Label>
                                                                <Form.Control {...text('emergency_contact_number2')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>緊急聯絡人(3)</Form.Label>
                                                                <Form.Control {...text('emergency_contact3')} type="text" placeholder="請輸入聯絡人名稱" />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number3">
                                                                <Form.Label>緊急聯絡人號碼(3)</Form.Label>
                                                                <Form.Control {...text('emergency_contact_number3')} type="text" placeholder="請輸入聯絡人號碼" maxLength={8} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>覆診醫生</Form.Label>
                                                                <Form.Control {...text('doctor_in_charge')} type="text" placeholder="請輸入覆診醫生名稱" />
                                                            </Form.Group>
                                                            <fieldset className="genderField">
                                                                <Form.Group as={Row}>
                                                                    <Form.Label as="legend" column sm={10}>
                                                                        性別
                                                                    </Form.Label>
                                                                    <Col sm={10}>
                                                                        <input {...radio('gender', '男')} type="radio" value="男" />男
                                                                        <div></div>
                                                                        <input {...radio('gender', '女')} type="radio" value='女' />女
                                                                    </Col>
                                                                </Form.Group>
                                                            </fieldset>
                                                        </Form.Row>
                                                        {/* <Form.Row className="uploadFileRow">
                                                <fieldset>
                                                    <Form.Group>
                                                        <Form.File className="uploadFileForm" id="formcheck-api-regular">
                                                            <Form.File.Label>頭像</Form.File.Label>
                                                            <Form.File.Input />
                                                        </Form.File>
                                                    </Form.Group>
                                                </fieldset>
                                            </Form.Row> */}
                                                        <Form.Row>
                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                <Form.Label >藥物敏感</Form.Label>
                                                                <Form.Control {...text('drug_allergy')} as="textarea" rows={15} />
                                                            </Form.Group>
                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                <Form.Label>食物敏感</Form.Label>
                                                                <Form.Control {...text('food_allergy')} as="textarea" rows={15} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                <Form.Label>服用藥物</Form.Label>
                                                                <Form.Control {...text('drug_history')} as="textarea" rows={15} />
                                                            </Form.Group>
                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                <Form.Label>病歷資料</Form.Label>
                                                                <Form.Control {...text('medical_history')} as="textarea" rows={15} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group className="textInputArea" controlId="exampleForm.ControlTextarea1">
                                                                <Form.Label>住院紀錄</Form.Label>
                                                                <Form.Control {...text('hospital_history')} as="textarea" rows={15} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>血壓上壓上限</Form.Label>
                                                                <Form.Control {...text('default_systolic_blood_pressure_top_level')} type="text" placeholder="請輸入血壓上壓上限" maxLength={3} />
                                                            </Form.Group>

                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                                <Form.Label>血壓上壓下限</Form.Label>
                                                                <Form.Control {...text('default_systolic_blood_pressure_low_level')} type="text" placeholder="請輸入血壓上壓下限" maxLength={3} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>血壓下壓上限</Form.Label>
                                                                <Form.Control {...text('default_diastolic_blood_pressure_top_level')} type="text" placeholder="請輸入血壓下壓上限" maxLength={3} />
                                                            </Form.Group>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                                <Form.Label>血壓下壓下限</Form.Label>
                                                                <Form.Control {...text('default_diastolic_blood_pressure_low_level')} type="text" placeholder="請輸入血壓下壓下限" maxLength={3} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>血氧上限</Form.Label>
                                                                <Form.Control {...text('default_blood_oxygen_contact_top_level')} type="text" placeholder="請輸入血氧上限" maxLength={3} />
                                                            </Form.Group>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                                <Form.Label>血氧下限</Form.Label>
                                                                <Form.Control {...text('default_blood_oxygen_contact_low_level')} type="text" placeholder="請輸入血氧下限" maxLength={3} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <Form.Row>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact">
                                                                <Form.Label>心率上限</Form.Label>
                                                                <Form.Control {...text('default_heart_rate_top_level')} type="text" placeholder="請輸入心率上限" maxLength={3} />
                                                            </Form.Group>
                                                            <Form.Group as={Col} controlId="formGridEmergency_contact_number2">
                                                                <Form.Label>心率下限</Form.Label>
                                                                <Form.Control {...text('default_heart_rate_low_level')} type="text" placeholder="請輸入心率下限" maxLength={3} />
                                                            </Form.Group>
                                                        </Form.Row>
                                                    </div>
                                                </Container>
                                            </div >
                                        </Row>
                                    </Container>
                                </ListGroupItem>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={() => setEditing(null)}>
                                        取消
                                    </Button>
                                    <Button variant="primary" type="submit" >
                                        儲存
                                    </Button>
                                </Modal.Footer>
                            </form>
                        </Modal>
                    </ListGroupItem>
                </ListGroup>

            </div>
        </div>





    )

}


export default UserDetailPage;