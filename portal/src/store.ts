import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { RouterState, RouterAction, connectRouter, routerMiddleware } from "connected-react-router"
import { createBrowserHistory } from 'history';
import thunk, {ThunkDispatch as OldThunkDispatch} from 'redux-thunk';
import {AuthState,authReducer} from './redux/auth/reducer'
import { AuthActions } from "./redux/auth/actions";
import { FollowupState, followupReducer } from "./redux/followup/reducer";
import { colleagueReducer, ColleagueState } from "./redux/colleague/reducer";
import { FollowupActions } from "./redux/followup/actions";
import { ColleagueActions } from "./redux/colleague/actions";
import { BpHistoryState, bpHistoryReducer } from "./redux/bp_history/reducer";
import { BpHistoryActions } from "./redux/bp_history/actions";
import { userPageState, userPageReducer } from "./redux/userManage/reducer";
import { UserPageActions } from "./redux/userManage/actions";
import { RemarkActions } from "./redux/remark/actions";
import { RemarkState, remarkReducer } from "./redux/remark/reducer";

export const history = createBrowserHistory();

export interface RootState {
    remark:RemarkState,
    user_page:userPageState,
    bp_history:BpHistoryState,
    auth:AuthState,
    router: RouterState,
    followup: FollowupState
    colleague: ColleagueState
}

export type RootActions = RouterAction | AuthActions | FollowupActions | ColleagueActions |BpHistoryActions | UserPageActions | RemarkActions;

const reducers = combineReducers<RootState>({
    remark:remarkReducer,
    user_page:userPageReducer,
    bp_history:bpHistoryReducer,
    followup: followupReducer,
    auth: authReducer,
    colleague: colleagueReducer,
    //authentication
    router: connectRouter(history),
})



declare global{
    /* tslint:disable:interface-name */
    interface Window{
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}

export type ThunkDispatch = OldThunkDispatch<RootState,SVGFEGaussianBlurElement,RootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export const store = createStore(reducers,composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))

))