import React from 'react';
import {RouteProps,Route, Redirect} from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from './store';

export function PrivateRoute(props:RouteProps){
    const isAuthenticated = useSelector((state: RootState ) => state.auth.isAuthenticated)
    const render = () => {
        return <Redirect to={{
            pathname: '/',
            state:{ from:props.path}
        }} />
    }
    if(isAuthenticated){
        return <Route {...props}/>

    }else{
        const {children, component, ...remainingProps} = props;
        return <Route {...remainingProps} render={render}/>
    }
}