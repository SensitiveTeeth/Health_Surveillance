import { Followup, FollowupActions } from "./actions";


export interface FollowupState {
    followupIds: number[];
    followupCount: number
    followupById: {
        [id: string]: Followup
    }
}

const initialState: FollowupState = {
    followupIds: [],
    followupCount: 0,
    followupById: {},
}
export const perPage = 7

export function followupReducer(state: FollowupState = initialState, action: FollowupActions): FollowupState {
    switch (action.type) {
        case '@@follow_up/LOADED_FOLLOW_UP':
            {
                const newFollowpById: {
                    [id: string]: Followup
                } = { ...state.followupById }
                const newFollowupIds = state.followupIds.slice();
                let i = 0;
                for (const followup of action.followup) {
                    newFollowpById[followup.id] = followup;
                    newFollowupIds[perPage * (action.page - 1) + i] = followup.id
                    i++
                }

                return {
                    ...state,
                    followupById: newFollowpById,
                    followupIds: newFollowupIds,
                    followupCount: action.count
                }
            }
        default:
            return state;
    }
}