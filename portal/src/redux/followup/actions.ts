import { ThunkDispatch, RootState } from "../../store";

export interface Followup {
    id:                                number;
    chinese_name:                      string;
    measured_systolic_blood_pressure:  string;
    measured_diastolic_blood_pressure: string;
    measured_blood_oxygen_contact:     string;
    measured_heart_rate:               string;
    date:                              string;
    english_name:                      string;
    gender:                            string;
}



export function loaded_Followup(followup:Followup[],count: number,page: number) {
    return{
        type: '@@follow_up/LOADED_FOLLOW_UP' as '@@follow_up/LOADED_FOLLOW_UP' ,
        followup,
        page,
        count,

    }
}

export type FollowupActions = ReturnType<typeof loaded_Followup>;

//thunk

export function fetchFollowup(page:number){
    return async (dispatch:ThunkDispatch, getState:() => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/followup?page=${page}`,{
            headers:{
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        const json= await res.json();
        dispatch(loaded_Followup(json.normal_user,json.count[0].count,page))


    }
}