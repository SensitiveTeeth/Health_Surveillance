import { SingleRemark, RemarkActions } from "./actions";


export interface RemarkState {
    remarkIds:SingleRemark[];

}

const initialState: RemarkState = {
    remarkIds: [],
    // remarkCount: 0,
    // remarkById: {},
}
export const perPage = 5

export function remarkReducer(state: RemarkState = initialState, action: RemarkActions): RemarkState {
    switch (action.type) {
        case '@@remark/LOADED_REMARK':
            {
                return {
                    ...state,
                    remarkIds:action.remark
                }
            }
        default:
            return state;
    }
}