import { ThunkDispatch, RootState } from "../../store";

export interface SingleRemark {
    id:            number;
    users_id:      number;
    bp_history_id: number;
    text_remark:   string;
    created_at:    string;
    updated_at:    string;
}



export function loaded_Remark(remark:SingleRemark[]) {
    return{
        type: '@@remark/LOADED_REMARK' as '@@remark/LOADED_REMARK' ,
        remark,

    }
}

export type RemarkActions = ReturnType<typeof loaded_Remark>;

//thunk

export function fetchRemark(id: number){
    return async (dispatch:ThunkDispatch, getState:() => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/remark/${id}`,{
            headers:{
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        try{
            const json= await res.json();
            dispatch(loaded_Remark(json))
        }catch(e){
            console.error(e);

        }


    }
}