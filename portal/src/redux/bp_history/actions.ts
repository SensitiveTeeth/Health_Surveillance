import { ThunkDispatch, RootState } from "../../store";

export interface BpHistory {
    id:                                          number;
    chinese_name:                                string;
    measured_systolic_blood_pressure:            string;
    measured_diastolic_blood_pressure:           string;
    measured_blood_oxygen_contact:               string;
    measured_heart_rate:                         string;
    created_at:                                  string;
    measured_systolic_blood_pressure_out_range:  null;
    measured_diastolic_blood_pressure_out_range: null;
    measured_blood_oxygen_contact_out_range:     null;
    measured_heart_rate_out_range:               null;
    needed_to_follow_up_list:                    null;
    been_followed:                               null;
}




export function loaded_BpHistory(bpHistory:BpHistory[],count:number,page:number) {
    return{
        type: '@@bp_history/LOADED_BP_HISTORY' as '@@bp_history/LOADED_BP_HISTORY',
        bpHistory,
        page,
        count,

    }
}

export type BpHistoryActions = ReturnType<typeof loaded_BpHistory>;

//thunk

export function fetchBpHistory(page:number,id:number){
    
    return async (dispatch:ThunkDispatch, getState:() => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/bp_history/${id}/?page=${page}`,{
            headers:{
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        try{
            const json= await res.json();
            dispatch(loaded_BpHistory(json.bp_history,parseInt(json.count[0].count),page))
        }catch(e){
            console.error(e);
        }
        
        
    }
}