import { BpHistory, BpHistoryActions } from "./actions";


export interface BpHistoryState {
    bpHistoryIds: number[];
    bpHistoryCount: number
    bpHistoryById: {
        [id: string]: BpHistory
    }
}

const initialState: BpHistoryState = {
    bpHistoryIds: [],
    bpHistoryCount: 0,
    bpHistoryById: {},
}
export const perPage = 5

export function bpHistoryReducer(state: BpHistoryState = initialState, action: BpHistoryActions): BpHistoryState {
    switch (action.type) {
        case '@@bp_history/LOADED_BP_HISTORY':
            {
                const newBpHistoryById: {
                    [id: string]: BpHistory
                } = { ...state.bpHistoryById }
                const newBpHistoryIds = state.bpHistoryIds.slice();
                let i = 0;
                for (const bp_history of action.bpHistory) {
                    newBpHistoryById[bp_history.id] = bp_history;
                    newBpHistoryIds[perPage * (action.page - 1) + i] = bp_history.id
                    i++
                }

                return {
                    ...state,
                    bpHistoryById: newBpHistoryById,
                    bpHistoryIds: newBpHistoryIds,
                    bpHistoryCount: action.count
                }
            }
        default:
            return state;
    }
}