import { ThunkDispatch, RootState } from "../../store";

export interface Colleague {
id:         number;
username:   string;
position:   string;
usercode:   string;
}

export function loaded_Colleague(colleague:Colleague[],count:number,page:number) {
    return{
        type: '@@colleague/LOADED_COLLEAGUE' as '@@colleague/LOADED_COLLEAGUE' ,
        colleague,
        page,
        count,
    }
}

export type ColleagueActions = ReturnType<typeof loaded_Colleague>;

//thunk

export function fetch_Colleague(page:number){
    return async (dispatch:ThunkDispatch, getState:() => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/colleague?page=${page}`,{
            headers:{
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        const json= await res.json();
        dispatch(loaded_Colleague(json.colleague,json.count[0].count,page))


    }
}
