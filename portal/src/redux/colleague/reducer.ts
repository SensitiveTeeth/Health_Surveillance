import { Colleague, ColleagueActions } from "./actions";


export interface ColleagueState {
    colleagueIds: number[];
    colleagueCount: number
    colleagueById: {
        [id: string]: Colleague
    }
}

const initialState: ColleagueState = {
    colleagueIds: [],
    colleagueCount: 0,
    colleagueById: {},
}
export const perPage = 7

export function colleagueReducer(state: ColleagueState = initialState, action: ColleagueActions): ColleagueState {
    switch (action.type) {
        case '@@colleague/LOADED_COLLEAGUE':
            {
                const newColleagueById: {
                    [id: string]: Colleague
                } = { ...state.colleagueById }
                const newColleagueIds = state.colleagueIds.slice();
                let i = 0;
                for (const colleague of action.colleague) {
                    newColleagueById[colleague.id] = colleague;
                    newColleagueIds[perPage * (action.page - 1) + i] = colleague.id
                    i++
                }

                return {
                    ...state,
                    colleagueById: newColleagueById,
                    colleagueIds: newColleagueIds,
                    colleagueCount: action.count
                }
            }
        default:
            return state;
    }
}