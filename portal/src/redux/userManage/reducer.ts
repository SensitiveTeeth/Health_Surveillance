import { UserPage, UserPageActions } from "./actions";


export interface userPageState {
    userPageIds: number[];
    userPageCount: number
    userPageById: {
        [id: string]: UserPage
    }
}

const initialState: userPageState = {
    userPageIds: [],
    userPageCount: 0,
    userPageById: {},
}
export const perPage = 7

export function userPageReducer(state: userPageState = initialState, action: UserPageActions): userPageState {
    switch (action.type) {
        case '@@UserPage/LOADED_USER_PAGE':
            {
                const newUserPageById: {
                    [id: string]: UserPage
                } = { ...state.userPageById }
                const newFollowupIds = state.userPageIds.slice();
                let i = 0;
                for (const user_page of action.user_page) {
                    newUserPageById[user_page.id] = user_page;
                    newFollowupIds[perPage * (action.page - 1) + i] = user_page.id
                    i++
                }

                return {
                    ...state,
                    userPageById: newUserPageById,
                    userPageIds: newFollowupIds,
                    userPageCount: action.count
                }
            }
        default:
            return state;
    }
}