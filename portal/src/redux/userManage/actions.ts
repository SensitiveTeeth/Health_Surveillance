import { ThunkDispatch, RootState } from "../../store";

export interface UserPage {
    id:           number;
    chinese_name: string;
    english_name: string;
    gender:       string;
}


export function loaded_UserPage(user_page: UserPage[],count: number, page: number) {
    return {
        type: '@@UserPage/LOADED_USER_PAGE' as '@@UserPage/LOADED_USER_PAGE' ,
        user_page,
        page,
        count,
    }
}

export type UserPageActions = ReturnType<typeof loaded_UserPage>;

//thunk

export function fetchUserPage(page: number) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user_page?page=${page}`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        const json = await res.json();
        dispatch(loaded_UserPage(json.user_page,json.count[0].count,page))


    }
}
