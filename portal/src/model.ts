export interface Normal_User {
    id:                                         number;
    usercode:                                   string;
    chinese_name:                               string;
    english_name:                               string;
    home_number:                                string;
    contact_person:                             string;
    contact_person_number:                      string;
    emergency_contact:                          string;
    emergency_contact_number:                   string;
    emergency_contact2:                         string;
    emergency_contact_number2:                  string;
    emergency_contact3:                         string;
    emergency_contact_number3:                  string;
    gender:                                     string;
    doctor_in_charge:                           string;
    drug_allergy:                               string;
    food_allergy:                               string;
    drug_history:                               string;
    medical_history:                            string;
    hospital_history:                           string;
    default_systolic_blood_pressure_top_level:  string;
    default_systolic_blood_pressure_low_level:  string;
    default_diastolic_blood_pressure_top_level: string;
    default_diastolic_blood_pressure_low_level: string;
    default_blood_oxygen_contact_top_level:     string;
    default_blood_oxygen_contact_low_level:     string;
    default_heart_rate_top_level:               string;
    default_heart_rate_low_level:               string;
    created_at:                                 string;
    updated_at:                                 string;
}





export const initialNormalUser = {
    id: 0,
    usercode: '',
    home_number: '',
    chinese_name: '',
    english_name: '',
    contact_person: '',
    contact_person_number: '',
    emergency_contact: '',
    emergency_contact_number: '',
    emergency_contact2: '',
    emergency_contact_number2: '',
    emergency_contact3: '',
    emergency_contact_number3: '',
    doctor_in_charge: '',
    gender: '',
    drug_allergy: '',
    food_allergy: '',
    drug_history: '',
    medical_history: '',
    hospital_history: '',
    default_systolic_blood_pressure_top_level: '',
    default_systolic_blood_pressure_low_level: '',
    default_diastolic_blood_pressure_top_level: '',
    default_diastolic_blood_pressure_low_level: '',
    default_blood_oxygen_contact_top_level: '',
    default_blood_oxygen_contact_low_level: '',
    default_heart_rate_top_level: '',
    default_heart_rate_low_level: '',
    created_at: '',
    updated_at: '',
}

export const initialBpHistory = {
        id:                                          0,
        chinese_name:                                '',
        measured_systolic_blood_pressure:            '',
        measured_diastolic_blood_pressure:           '',
        measured_blood_oxygen_contact:               '',
        measured_heart_rate:                         '',
        created_at:                                  '',
        measured_systolic_blood_pressure_out_range:  null,
        measured_diastolic_blood_pressure_out_range: null,
        measured_blood_oxygen_contact_out_range:     null,
        measured_heart_rate_out_range:               null,
        needed_to_follow_up_list:                    null,
        been_followed:                               null,
    }